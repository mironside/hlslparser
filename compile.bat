if not "%VCVARS%"=="msvc14" (
	set VCVARS=msvc14
	call "%VS140COMNTOOLS%..\..\VC\vcvarsall.bat" x64
)

mkdir obj 2> nul
mkdir bin 2> nul

cl.exe -W4 -WX -wd4100 -wd4206 -wd4115 -nologo -FS -c -GR- -EHa- -Ox -GL -Fdbin\hlslparser -MD -EHsc -Foobj\ -Isrc -D_WIN32_WINNT=0x0601 -D_CRT_SECURE_NO_WARNINGS src\build.cpp || (goto :build_failed)
link.exe -nologo -MACHINE:X64 -DEBUG -subsystem:console -out:bin\hlslparser.exe -LIBPATH:obj\ build.obj Ole32.lib Gdi32.lib User32.lib || (goto :build_failed)
